import paho.mqtt.client as mqtt
import time
import math
import argparse
import requests
import random

# MQTT Broker settings
broker_address = "127.0.0.1"
broker_port = 1883

# IoT Agent settings
iot_agent_address = "127.0.0.1"

# MQTT Client settings
client = mqtt.Client(mqtt.CallbackAPIVersion.VERSION2)

lamp_offsets = {}  # Dictionary to store offsets for each lamp
step_size = 10

def check_health():
    try:
        response = requests.get(f"http://{iot_agent_address}:4041/iot/about")
        if response.status_code == 200:
            print("Healthcheck passed. IoT Agent is running.")
            return True
        else:
            print("Healthcheck failed. IoT Agent is not running or unreachable.")
            return False
    except Exception as e:
        print("Error during healthcheck:", e)
        return False

def on_connect(client, userdata, flags, reason_code, properties):
    print("Connected with reason code "+str(reason_code))

def on_message(client, userdata, msg):
    print("Message received on topic "+msg.topic+": "+str(msg.payload))

def generate_value(lamp_number):
    global lamp_offsets

    lamp_number = str(lamp_number)

    if lamp_number in lamp_offsets:
        offset = lamp_offsets[lamp_number]  # Unique offset for each lamp
    else:
        offset = random.random()  # Generate a random offset if lamp_number is not in lamp_offsets
        lamp_offsets[lamp_number] = offset
    value = (counter + offset) % 11  # Ensure the value stays within the range [0, 10]
    print(lamp_offsets)
    return value * 10


def simulate_data(lamp_number):
    global counter
    publish_topic_1 = f"/TEF/lamp{lamp_number}/attrs"
    publish_topic_2 = f"/TEF/lamp{lamp_number}/attrs/l"

    value = generate_value(lamp_number)

    # Randomly change lamp state after a random amount of time
    if random.random() < 0.05:  # Probability of state change
        new_state = "off" if random.choice([True, False]) else "on"
        client.publish(publish_topic_1, f"s|{new_state}")
        print(f"Lamp {lamp_number} state changed to {new_state}")

    client.publish(publish_topic_2, value)
    print(f"Published simulated data for Lamp {lamp_number} - Luminosity:", value)

def main(start_lamp, end_lamp):
    global counter
    counter = 0

    if not check_health():
        return

    client.on_connect = on_connect
    client.on_message = on_message
    client.connect(broker_address, broker_port, 60)

    # Loop to keep the client running and publishing simulated data
    client.loop_start()
    while True:
        for lamp_number in range(start_lamp, end_lamp + 1):
            simulate_data(f'{lamp_number:03d}')
        counter += 1  # Increment the counter for the next iteration

        time.sleep(2)

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Simulate MQTT data for lamps in a range.")
    parser.add_argument("start_lamp", type=int, help="The number of the starting lamp in the range.")
    parser.add_argument("end_lamp", type=int, help="The number of the ending lamp in the range.")
    args = parser.parse_args()
    main(args.start_lamp, args.end_lamp)

