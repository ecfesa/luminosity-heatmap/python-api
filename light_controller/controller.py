import requests
import json
import time
from datetime import datetime, timedelta, timezone
import sys


# orion_address = '10.5.10.34:1026'
# iot_agent_address = '10.5.10.34:4041'
# sth_address = '10.5.10.34:8666'
orion_address = 'localhost:1026'
iot_agent_address = 'localhost:4041'
sth_address = 'localhost:8666'

check_interval_seconds = 2

def get_lamp_state(entity_name):
    url = f"http://{orion_address}/v2/entities/{entity_name}/attrs/state"

    payload = {}
    headers = {
        'fiware-service': 'smart',
        'fiware-servicepath': '/',
        'accept': 'application/json'
    }

    response = requests.request("GET", url, headers=headers, data=payload)
    return response.json()['value'] == 'on'


def get_last_minute():
    current_time = datetime.now(timezone.utc) - timedelta(minutes=1)
    return current_time.strftime("%Y-%m-%dT%H:%M:%S.%fZ")


def get_now():
    current_time = datetime.now(timezone.utc)
    return current_time.strftime("%Y-%m-%dT%H:%M:%S.%fZ")


def get_minute_average(entity_name):
    time_from = get_last_minute()
    time_to = get_now()
    url = f"http://{sth_address}/STH/v2/entities/{entity_name}/attrs/luminosity?type=Lamp&dateFrom={time_from}&dateTo={time_to}&lastN=100"
    headers = {'fiware-service': 'smart', 'fiware-servicepath': '/'}
    response = requests.get(url, headers=headers)
    if response.status_code == 200:
        data = response.json()
        luminosity_values = [entry['attrValue'] for entry in data['value']]
        return sum(luminosity_values) / len(luminosity_values) if luminosity_values else 0
    else:
        return 0


def set_state(state, entity_name):
    url = f"http://{orion_address}/v2/entities/{entity_name}/attrs"
    payload = json.dumps({f"{'on' if state else 'off'}": {"type": "command", "value": ""}})
    headers = {'Content-Type': 'application/json', 'fiware-service': 'smart', 'fiware-servicepath': '/'}
    response = requests.patch(url, headers=headers, data=payload)

# Returns true if the state was changed
def calculate_state(luminosity, current_state, entity_name):
    if luminosity > 50 and current_state == True:
        set_state(False, entity_name)
        return True
    elif luminosity <= 50 and current_state == False:
        set_state(True, entity_name)
        return True
    else:
        return False

def get_devices():
    url = f"http://{iot_agent_address}/iot/devices"

    payload = {}
    headers = {
    'fiware-service': 'smart',
    'fiware-servicepath': '/'
    }

    response = requests.request("GET", url, headers=headers, data=payload)
    devices = []

    for device in response.json()['devices']:
        devices.append(device['entity_name'])
    
    return devices


def main():
    print("Started light controller. Controlling all lights available.\n\n")

    while True:
        print("---------------------")

        lamps = get_devices()
        for entity_name in lamps:
            luminosity_average = get_minute_average(entity_name)
            lamp_state = get_lamp_state(entity_name)
            state_changed = calculate_state(luminosity_average, lamp_state, entity_name)
            state_changed_txt = ""
            if state_changed and lamp_state == True:
                state_changed_txt = "Turned lamp OFF"
            elif state_changed and lamp_state == False:
                state_changed_txt = "Turned lamp ON"

            print(f'lamp {entity_name}: {"ON" if lamp_state else "off"} | luminosity={luminosity_average}{"| "+state_changed_txt if state_changed != "" else ""}')
        time.sleep(check_interval_seconds)


if __name__ == "__main__":
    main()
