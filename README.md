# Luminosity Heatmap Python API

A simple python API that consumes data from the fiware back-end stack (Orion context broker, STH Comet and IOT Agent) and returns it processed in a simple JSON format

## Available APIs


### Luminosity averages

`GET /api/lamps/averages`: Returns a full list of the lamps with their respective luminosity averages

Sample response body:
```
{
  "urn:ngsi-ld:Lamp:001": 50.58097860816667,
  "urn:ngsi-ld:Lamp:002": 50.58190091475,
  "urn:ngsi-ld:Lamp:003": 50.58236872933333,
  "urn:ngsi-ld:Lamp:004": 50.58283531408333,
  "urn:ngsi-ld:Lamp:005": 50.58307634533333,
  "urn:ngsi-ld:Lamp:006": 50.58328364383333,
  "urn:ngsi-ld:Lamp:007": 50.58367614358334,
  "urn:ngsi-ld:Lamp:008": 50.58403167183334
}
```

### State of all lamps

`GET /api/lamps/states`: Returns a full list of lamps with their current state (on/off)

Sample response body:
```
{
  "urn:ngsi-ld:Lamp:001": true,
  "urn:ngsi-ld:Lamp:002": true,
  "urn:ngsi-ld:Lamp:003": false,
  "urn:ngsi-ld:Lamp:004": true,
  "urn:ngsi-ld:Lamp:005": false,
  "urn:ngsi-ld:Lamp:006": false,
  "urn:ngsi-ld:Lamp:007": true,
  "urn:ngsi-ld:Lamp:008": true
}
```
