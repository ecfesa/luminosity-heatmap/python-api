from flask import Flask, jsonify
from flask_cors import CORS

import requests
import json
import time
from datetime import datetime, timedelta, timezone
import threading

app = Flask(__name__)
CORS(app, resources={r"/api/*": {"origins": "*"}})

# orion_address = '10.5.10.34:1026'
# iot_agent_address = '10.5.10.34:4041'
# sth_address = '10.5.10.34:8666'
orion_address = 'localhost:1026'
iot_agent_address = 'localhost:4041'
sth_address = 'localhost:8666'

data_update_interval_seconds = 30
saved_data = {'lamp_averages': {}, 'lamp_states': {}}
lock = threading.Lock()

def get_last_minute():
    current_time = datetime.now(timezone.utc) - timedelta(minutes=1)
    return current_time.strftime("%Y-%m-%dT%H:%M:%S.%fZ")

def get_now():
    current_time = datetime.now(timezone.utc)
    return current_time.strftime("%Y-%m-%dT%H:%M:%S.%fZ")

def get_minute_average(entity_name):
    time_from = get_last_minute()
    time_to = get_now()
    url = f"http://{sth_address}/STH/v2/entities/{entity_name}/attrs/luminosity?type=Lamp&dateFrom={time_from}&dateTo={time_to}&lastN=100"
    headers = {'fiware-service': 'smart', 'fiware-servicepath': '/'}
    response = requests.get(url, headers=headers)
    if response.status_code == 200:
        data = response.json()
        luminosity_values = [entry['attrValue'] for entry in data['value']]
        return sum(luminosity_values) / len(luminosity_values) if luminosity_values else 0
    else:
        return 0

def get_lamp_state(entity_name):
    url = f"http://{orion_address}/v2/entities/{entity_name}/attrs/state"

    payload = {}
    headers = {
    'fiware-service': 'smart',
    'fiware-servicepath': '/',
    'accept': 'application/json'
    }

    response = requests.request("GET", url, headers=headers, data=payload)
    return response.json()['value'] == 'on'

def get_devices():
    url = f"http://{iot_agent_address}/iot/devices"

    payload = {}
    headers = {
        'fiware-service': 'smart',
        'fiware-servicepath': '/'
    }

    try:
        response = requests.request("GET", url, headers=headers, data=payload)
    except Exception as e:
        return []

    devices = []

    for device in response.json()['devices']:
        devices.append(device['entity_name'])

    return devices

def update_saved_data():
    global saved_data
    global lock
    
    while True:
        lamps = get_devices()
        new_data = {'lamp_averages': {}, 'lamp_states': {}}
        
        new_data['lamp_averages'] = {}
        new_data['lamp_states'] = {}
        
        for entity_name in lamps:
            new_data['lamp_averages'][entity_name] = get_minute_average(entity_name)
            new_data['lamp_states'][entity_name] = get_lamp_state(entity_name)

        with lock:
            saved_data = new_data
        
        time.sleep(data_update_interval_seconds)

# Start the background thread to update saved data
update_thread = threading.Thread(target=update_saved_data)
update_thread.daemon = True
update_thread.start()

@app.route('/api/lamps/averages', methods=['GET'])
def get_lamp_averages():
    global saved_data
    with lock:
        return jsonify(saved_data['lamp_averages'])

@app.route('/api/lamps/states', methods=['GET'])
def get_lamp_states():
    global saved_data
    with lock:
        return jsonify(saved_data['lamp_states']) 

if __name__ == '__main__':
    app.run(debug=True)
